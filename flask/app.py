from flask import Flask

import os


app = Flask(__name__)


@app.route("/")
def hello():
	return "<h1>Hi, This is TS4U changes. Adding a new change.</h1>"

if __name__ == "__main__":
	port = int(os.environ.get("PORT",5000))
	app.run(debug=True, host='0.0.0.0', port=port)

